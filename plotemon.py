#!/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import os
import sys

level = np.arange(1,40.5,0.5)

for scatter_file in sys.argv[1:]:
    if not os.path.isfile(scatter_file):
        continue
    vals = np.loadtxt(scatter_file)
    aVal = 0.8
    s = 7

    lowerlim = 1
    upperlim = 40
    xlabel = 'Level'

    plt.subplot(2,1,1)
    try:
        plt.scatter(level, vals[:,0], s, alpha=aVal)
    except IndexError:
        continue;
    except Exception as e:
        raise e
                        
    plt.xlim(lowerlim, upperlim)
    plt.ylabel('Candy')
    plt.grid(True)

    plt.subplot(2,1,2)
    try:
        plt.scatter(level, vals[:,1], s, alpha=aVal)
    except IndexError:
        continue;
    except Exception as e:
        raise e
    plt.xlim(lowerlim, upperlim)
    plt.ylabel('Stardust')
    plt.grid(True)

    plt.show()
